#!/usr/bin/perl
use warnings;
use strict;
use ClassA;

my $o = ClassA->new();
print $o->name(), "\n";

my $p = ClassA->new("meg");
print $p->name(), "\n";

my $q = ClassA->new(name => "ndlp-ng");
print $q->name(), "\n";
