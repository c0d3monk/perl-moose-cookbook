package RoleB;

use Moose::Role;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str'
    );

sub toString {
    my $self = shift;
    return $self->name();
}

1;
