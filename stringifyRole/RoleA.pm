package RoleA;

use Moose::Role;
use overload q("") => "toString";

has 'role' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

sub action {
    my $self = shift;
    print "I am a $self->role() \n";
}

1;
