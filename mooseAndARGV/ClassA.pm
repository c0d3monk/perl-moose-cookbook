package ClassA;

use Data::Dumper;
use Moose;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str'
    );

sub BUILD {
    my $self = shift;
    my @cmdArgs = @ARGV;
    print Dumper(\@cmdArgs), "\n";
}

1;
