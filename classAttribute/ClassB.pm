package ClassB;

use Moose;

has 'init' => (
    'is' => 'rw',
    'isa' => 'Int',
    );

sub data {
    my $self = shift;
    if (ClassA->debug()) {
        print "Debug::\n";
    }
    print "Data\n";
}

1;
