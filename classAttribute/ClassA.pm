package ClassA;

use Moose;
use MooseX::ClassAttribute;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

class_has 'debug' => (
    'is' => 'rw',
    'isa' => 'Bool',
    'default' => 0,
    );

1;
