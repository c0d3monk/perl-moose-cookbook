package Role2;

use Moose::Role;
with 'Role1';

sub action {
    my $self = shift;
    return "I do an action.\n";
}

1;
