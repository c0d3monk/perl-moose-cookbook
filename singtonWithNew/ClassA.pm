package ClassA;

use MooseX::Singleton;
with 'RoleA';

has 'time' => (
    'is' => 'rw',
    'isa' => 'Str',
    'default' => localtime(),
    );


1;
