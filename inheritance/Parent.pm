package Parent;

use Moose;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str',
    );

has 'value' => (
    'is' => 'rw',
    'isa' => 'Int'
    );

sub square {
    my $self = shift;
    $self->value($self->value * $self->value);
}

1;
