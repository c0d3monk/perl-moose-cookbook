package ClassA;

use Moose;

has 'name' => (
    'is' => 'rw',
    'isa' => 'Str'
    );

has 'version' => (
    'is' => 'rw',
    'isa' => 'Int'
    );

sub nextVersion {
    my $self = shift;
    return $self->version() + 1;
}

my $meta = __PACKAGE__->meta;
for my $method ($meta->get_method_list()) {
    before "$method" => sub {
        print "Entering $method\n";
    };
    after "$method" => sub {
        print "Exiting $method\n";
    };
}

    
1;
