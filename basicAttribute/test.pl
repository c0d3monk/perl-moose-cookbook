#!/usr/bin/perl
use warnings;
use strict;
use ClassA;

# Basic.
my $o = ClassA->new('name' => 'Someone', 'age' => 30);
print "Name: " . $o->name() . "\n";
print "Age: " . $o->age() . "\n";
print "Details ?". $o->has_details() . "\n";

# Type validation.
# my $o = ClassA->new('name' => 'Someone', 'age' => 30, emails => 'user@domain');
# print "@{$o->emails()}" , "\n";

# Init order.
# my $o = ClassA->new('name' => 'Someone', 'age' => 30, emails => ['user@domain'], detailsFile => '/tmp/details.xml');
# print "@{$o->emails()}" , "\n";

# my $o = ClassA->new('name' => 'Someone', 'age' => 30, emails => ['user@domain'], detailsFile => '/tmp/details.xml');
# $o->vehicle(1);
# $o->vehicle(0);
# $o->addEmail('my@email.address');
# print "@{$o->emails()}" , "\n";
